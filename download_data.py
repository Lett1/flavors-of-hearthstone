# -*- coding: utf-8 -*-
import json
import requests
from queue import Queue
from threading import Thread
import itertools
import pathlib

from bs4 import BeautifulSoup
from pprint import pprint

from secret import MASHAPE_KEY

def downloadCard(url, name):
    try:
        downloader.retrieve(url, name + ".png")
        time.sleep(2)
    except IOError:
        print("Could not get card: " + name)

def sanitize_name(name):
    return name.replace("!", "").replace(" ", "_").replace("'", "").lower()

def download_worker():
    while True:
        url, path = q.get()
        print("Saving %s to %s" % (url, path))
        r = requests.get(url, stream=True, headers={ "user-agent": "Mozilla/5.0 (compatible; http://lett.at/hsbrb)" })
        with open(path, 'wb') as f:
            for chunk in r.iter_content(1024):
                if chunk:
                    f.write(chunk)
        q.task_done()



def download_all_cards(q, data):

    for card in data:
        if card["cardSet"] != "Hero Skins" and "flavor" in card:
            if pathlib.Path("html/img/card_img/%s.png" % card["cardId"]).exists():
                continue
            else:
                q.put((card["img"], "html/img/card_img/%s.png" % card["cardId"]))

    q.join()  

def download_all_cardbacks(q, data):

    for cardback in data:
        name = sanitize_name(cardback["name"])
        #if cardback["cardSet"] != "Hero Skins" and "flavor" in card:
        if pathlib.Path("html/img/cardback_img/%s.png" % name).exists():
            continue
        else:
            q.put((cardback["img"], "html/img/cardback_img/%s.png" % name))

    q.join()  


def check_for_sound_file(card, type):
    return pathlib.Path("html/sounds/%s_%s.ogg" % (card["cardId"], type)).exists() or pathlib.Path("html/sounds/%s_%s.mp3" % (card["cardId"], type)).exists()

def download_all_sounds(q, data):

    for card in data:

        #Oh man thats a shitty if statement
        if card["rarity"] == "Legendary":
            if check_for_sound_file(card, "soundPlay1") and check_for_sound_file(card, "soundPlay2"):
                continue
        elif check_for_sound_file(card, "soundPlay1"):
            continue

        if card["cardSet"] != "Hero Skins" and "flavor" in card and card["type"] == "Minion":

            print("Getting sound for card %s" % card["name"])

            search_page = requests.get("http://www.hearthpwn.com/cards", 
                headers={ "user-agent": "Mozilla/5.0 (compatible; http://lett.at/hsbrb)"},
                params= {"filter-name": card["name"].replace("-"," ")})
            search_soup = BeautifulSoup(search_page.text, "html.parser")

            card_page_url = "http://www.hearthpwn.com" + search_soup.find_all("table", class_="listing")[0].a["href"]

            card_page = requests.get(card_page_url, 
                headers={ "user-agent": "Mozilla/5.0 (compatible; http://lett.at/hsbrb)"})
            card_soup = BeautifulSoup(card_page.text, "html.parser")

            audio_tags = [x for x in card_soup.find_all("audio") if "play" in x["id"].lower()]

            if len(audio_tags) > 1:
                audio_tags = [audio_tags[0], audio_tags[-1]]

            for audio in audio_tags:
                url = audio["src"]
                filepath = "html/sounds/%s_%s.ogg" % (card["cardId"], audio["id"])

                q.put((url, filepath))

    q.join()




if __name__ == "__main__":
    

    pathlib.Path('html/img/card_img').mkdir(parents=True, exist_ok=True) 
    pathlib.Path('html/img/cardback_img').mkdir(parents=True, exist_ok=True) 
    pathlib.Path('html/sounds').mkdir(parents=True, exist_ok=True) 
    pathlib.Path('html/data').mkdir(parents=True, exist_ok=True) 

    q = Queue()

    for i in range(10):
        print("Creating Worker thread %d" % i)
        t = Thread(target=download_worker)
        t.daemon = True
        t.start()

    response = requests.get("https://omgvamp-hearthstone-v1.p.mashape.com/cards",
      headers={
        "X-Mashape-Key": MASHAPE_KEY
      },
      params={
        "collectible":"1",
        "locale": "enUS"
      }
    )

    response.encoding = "utf-8"

    card_data = response.json()

    card_data = list(itertools.chain.from_iterable(card_data.values()))

    response = requests.get("https://omgvamp-hearthstone-v1.p.mashape.com/cardbacks",
      headers={
        "X-Mashape-Key": MASHAPE_KEY
      },
      params={
        "locale": "enUS"
      }
    )

    response.encoding = "utf-8"

    cardback_data = response.json()

    print("Starting Card Image Download")
    download_all_cards(q, card_data)

    print("Starting Sound Download")
    download_all_sounds(q, card_data)

    # TODO: Find better source for cardback images
    # print("Starting Cardback Download")
    # download_all_cardbacks(q, cardback_data)

    json_data = []

    print("Writing json data to file")
    for card in card_data:
        if "flavor" in card:
            json_data.append({"id":     card["cardId"],
                              "flavor": card["flavor"],
                              "type":   card["type"],
                              "artist": card["artist"],
                              "rarity": card["rarity"]})

    with open("html/data/cards.json", 'w') as f:
        f.write(json.dumps(json_data))


    
