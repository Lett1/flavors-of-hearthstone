// Two-sided card
// Three.js r.49 - Updated to r.62

var renderer, scene, camera, tl;
var card, cardData, cardbacks, currentCard;
var flavorText, artistText;
var bgmwidget;

var lastCards = [];


//dat.gui
var gui = new dat.GUI();

var params = {
    delay: 6,
    jumpToNextCard: function() { tl.seek(0);
        drawNewCard(); },
    paused: false,
    muteBgm: false,
    muteSfx: false
};

// ##     ## ######## #### ##
// ##     ##    ##     ##  ##
// ##     ##    ##     ##  ##
// ##     ##    ##     ##  ##
// ##     ##    ##     ##  ##
// ##     ##    ##     ##  ##
//  #######     ##    #### ########


Array.prototype.contains = function(element) {
    return this.indexOf(element) > -1;
};

function getRandomCardBack() {
    //return cardbacks[Math.floor(Math.random() * cardbacks.length)];
    return "classic";
}

function drawNewCard() {
    getRandomCard();
    setCard();
}

function getRandomCard() {
    do {
        currentCard = cardData[Math.floor(Math.random() * cardData.length)];
    } while (lastCards.contains(currentCard.id));
    lastCards.push(currentCard.id);
    if (lastCards.length > 10) {
        lastCards.shift();
    }
}


//  ######   ##     ## ####
// ##    ##  ##     ##  ##
// ##        ##     ##  ##
// ##   #### ##     ##  ##
// ##    ##  ##     ##  ##
// ##    ##  ##     ##  ##
//  ######    #######  ####

function initGui() {

    var searchParams = new URLSearchParams(window.location.search);

    if (searchParams.get("muteBgm") == "yes") {
        params.muteBgm = true;
        //TODO: Maybe destroy the iframe to stop it from actually doing stuff
    }

    if (searchParams.get("muteSfx") == "yes") {
        params.muteSfx = true;
        buzz.all().mute();
    }

    if (searchParams.get("hideGui") == "yes") {
        dat.GUI.toggleHide();
    }

    if (searchParams.get("delay")) {
        params.delay = parseInt(searchParams.get("delay"),10);
    }


    gui.add(params, 'delay', 1, 30).step(1).onFinishChange(function() {
        card.position.y = -500;
        tl.progress(0);
        tl.clear();
        //Card flies in from bottom
        tl.to(card.position, 2, { y: 0, ease: Quart.easeOut })
            //Card rotates 180deg
            .to(card.rotation, 2, { y: 0 })
            //Text fades in with rotation
            .to([flavorText, artistText], 0.3, { opacity: 1 }, "-=2")
            //Card flies out
            .to(card.position, 2, { y: 600, ease: Quart.easeIn, delay: params.delay, onComplete: drawNewCard })
            //and text fades with card
            .to([flavorText, artistText], 0.3, { opacity: 0 }, "-=2");
    });

    gui.add(params, 'jumpToNextCard').name("Draw a new card");

    gui.add(params, 'paused').name("Pause").onFinishChange(function() {
        if (params.paused) {
            tl.pause("cardFlyAway");
        } else {
            tl.play();
        }
    });

    gui.add(params, "muteSfx").name("Mute Sounds").onFinishChange(function() {
        if (params.muteSfx) {
            buzz.all().mute();
        } else {
            buzz.all().unmute();
        }
    });

    gui.add(params, "muteBgm").name("Mute Music").onFinishChange(function() {
        if (params.muteBgm) {
            bgmwidget.setVolume(0);
        } else {
            bgmwidget.setVolume(100);
        }
    });


}

// #### ##    ## #### ########
//  ##  ###   ##  ##     ##
//  ##  ####  ##  ##     ##
//  ##  ## ## ##  ##     ##
//  ##  ##  ####  ##     ##
//  ##  ##   ###  ##     ##
// #### ##    ## ####    ##


function init() {

    // renderer
    renderer = new THREE.WebGLRenderer({ alpha: true });
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // scene
    scene = new THREE.Scene();

    // camera
    camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 800;
    camera.lookAt(scene.position);
    camera.position.x = 200;

    window.addEventListener('resize', function() {
        var WIDTH = window.innerWidth,
            HEIGHT = window.innerHeight;
        renderer.setSize(WIDTH, HEIGHT);
        camera.aspect = WIDTH / HEIGHT;
        camera.updateProjectionMatrix();
    });

    // geometry
    var geometry1 = new THREE.PlaneGeometry(307, 465, 1, 1);

    var geometry2 = new THREE.PlaneGeometry(307, 465, 1, 1);
    geometry2.applyMatrix(new THREE.Matrix4().makeRotationY(Math.PI));

    // textures
    var textureFront = THREE.ImageUtils.loadTexture('img/default_front.png');
    var textureBack = THREE.ImageUtils.loadTexture('img/default_back.png');

    // material
    var material1 = new THREE.MeshBasicMaterial({ color: 0xffffff, map: textureFront });
    var material2 = new THREE.MeshBasicMaterial({ color: 0xffffff, map: textureBack, transparent: true });

    // card
    card = new THREE.Object3D();
    card.name = "card";
    scene.add(card);

    // mesh
    var mesh1 = new THREE.Mesh(geometry1, material1);
    mesh1.name = "front";
    card.add(mesh1);
    var mesh2 = new THREE.Mesh(geometry2, material2);
    mesh2.name = "back";
    card.add(mesh2);

    card.position.y = -500;
    card.rotation.y = Math.PI;


    flavorText = $(".flavor-text");
    artistText = $(".artist");

    //Make new timeline
    tl = new TimelineMax({ repeat: -1, repeatDelay: 1 });

    //Card flies in from bottom
    tl.to(card.position, 2, { y: 0, ease: Quart.easeOut })
        //Card rotates 180deg
        .to(card.rotation, 2, { y: 0, onStart: playSound })
        //Text fades in with rotation
        .to([flavorText, artistText], 0.3, { opacity: 1 }, "-=2")
        //Card flies out
        .to(card.position, 2, { y: 600, ease: Quart.easeIn, delay: 6, onComplete: drawNewCard }, "cardFlyAway")
        //and text fades with card
        .to([flavorText, artistText], 0.3, { opacity: 0 }, "-=2");

    tl.play()



}

// ##     ## ##     ##  ######  ####  ######
// ###   ### ##     ## ##    ##  ##  ##    ##
// #### #### ##     ## ##        ##  ##
// ## ### ## ##     ##  ######   ##  ##
// ##     ## ##     ##       ##  ##  ##
// ##     ## ##     ## ##    ##  ##  ##    ##
// ##     ##  #######   ######  ####  ######


function initMusic() {
    var widgetIframe = document.getElementById('bgm');
    bgmwidget = SC.Widget(widgetIframe);

    bgmwidget.bind(SC.Widget.Events.READY, function() {
        if (params.muteBgm) {
            bgmwidget.setVolume(0);
        }
        // load new widget
        bgmwidget.bind(SC.Widget.Events.FINISH, function() { //Play the sound again once it ended
            bgmwidget.play();
        });
    });
}

function playSound() {
    if (currentCard) {
        if (!params.muteSfx && currentCard.sound) {
            currentCard.sound.play();
        }

        if (!params.muteSfx && currentCard.legendSound) {
            currentCard.legendSound.play();
        }
    }
}

// ########     ###    ########    ###    
// ##     ##   ## ##      ##      ## ##   
// ##     ##  ##   ##     ##     ##   ##  
// ##     ## ##     ##    ##    ##     ## 
// ##     ## #########    ##    ######### 
// ##     ## ##     ##    ##    ##     ## 
// ########  ##     ##    ##    ##     ## 


function initJSON() {
    $.when(
        $.getJSON("data/cards.json", function(data) {
            cardData = data;
        })
    ).then(function() {
        if (cardData) {
            drawNewCard();
        } else {
            // Request for graphic data didn't work, handle it
        }
    });
}

function setCard() {
    var newFront = new THREE.ImageUtils.loadTexture("img/card_img/" + currentCard.id + ".png");
    newFront.minFilter = THREE.LinearFilter;

    var newBack = new THREE.ImageUtils.loadTexture("img/cardback_img/" + getRandomCardBack() + ".png");
    newBack.minFilter = THREE.LinearFilter;

    if (currentCard.type == "Minion") {
        currentCard.sound = new buzz.sound("sounds/" + currentCard.id + "_soundPlay1.mp3");
        if (currentCard.rarity == "Legendary") {
            currentCard.legendSound = new buzz.sound("sounds/" + currentCard.id + "_soundPlay2.mp3");
        } else {
            currentCard.legendSound = null;
        }
    } else {
        currentCard.sound = null;
    }

    card.getObjectByName("front").material.map = newFront;
    card.getObjectByName("back").material.map = newBack;
    flavorText.html(currentCard.flavor);
    artistText.html("Card art by " + currentCard.artist);
}



function animate() {

    requestAnimationFrame(animate);

    renderer.render(scene, camera);

}

init();
initJSON();
initGui();
initMusic();
animate();