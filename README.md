# Flavors of Hearthstone

Check it out on [lett.at/hsflavor](http://www.lett.at/hsflavor)

Three.js project that displays Hearthstone Cards and their assigned flavor texts. Originally thought of as an AFK screen for Hearthstone streamers or just something you look at once in a while. The GUI can be hidden with an url parameter, settings can also be configured by parameters.

![app-screenshot](http://lett.at/images/hsbrb.png)

## URL Configuration

The following parameters are accepted by the app: 

| Setting | Parameter | Description
| -------- | -------- | -------- |
| Hide GUI   | `hideGui=<yes-no>`   | Hides the GUI, useful for afk screens
| Delay   | `delay=<seconds>`   | Sets the delay between cards in seconds, default value 6s 
| Mute BGM   | `muteBgm=<yes-no>`   | Mutes the background Music
| Mute SFX   | `muteSfx=<yes-no>`   | Mutes the card sound effects

Parameters are placed after the url with a `?` and are seperated with `&`.
Example: [lett.at/hsflavor?hideGui=yes&delay=10](http://www.lett.at/hsflavor?hideGui=yes&delay=10) would hide the GUI and set the delay to 10 seconds.

## Self-Hosting

1. Clone this repo.
2. [Get a Mashape API Key](http://docs.mashape.com/api-keys) (Needed for the python script).
3. Create a file called `secret.py` and add your key like this: `MASHAPE_KEY="YOURKEY"`.
4. Install the required python packages (`pip install -r requirements.txt`).
5. Run `download_data.py` to retrieve images, sounds and card data.
6. Run `convert_sounds.sh` to convert sounds to mp3 (so we can play them on the web).
7. Copy the `html` folder wherever you want and open index.html in a webbrowser. (Hosting the folder in a webserver is recommended)